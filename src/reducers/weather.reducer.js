import { INPUT_ENTER_FIRED, INPUT_TEXT_CHANGE, WEATHER_FETCH_ERROR, WEATHER_FETCH_PENDING, WEATHER_FETCH_SUCCESS } from "../constants/weather.constant";

const initialState = {
    inputString: "",
    weather: {},
    pending: false,
    showResult: false
}

const weatherReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TEXT_CHANGE:
            state.inputString = action.payload;
            break;
        case WEATHER_FETCH_PENDING:
            state.pending = true;
            break;
        case WEATHER_FETCH_SUCCESS:
            state.pending = false;
            state.weather = action.data;
            break;
        case WEATHER_FETCH_ERROR:
            break;
        case INPUT_ENTER_FIRED:
            state.showResult = true;
            break;
        default:
            break;
    }

    return { ...state };
}

export default weatherReducer;
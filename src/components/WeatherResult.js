const WeatherResult = ({ wea }) => {
    console.log(wea);

    // Filter the forecasts to get only one forecast per day
    const uniqueForecastDays = [];
    const sixDaysForecast = wea.list.filter(forecast => {
        const forecastDate = new Date(forecast.dt_txt).getDate();
        if (!uniqueForecastDays.includes(forecastDate)) {
            return uniqueForecastDays.push(forecastDate);
        }
    });
    const fourDayForecast = sixDaysForecast.slice(1, 5);

    console.log(wea.list[0]);
    return (
        <div className="bg">
            <div className="content text-center">
                <div style={{ display: "flex", padding: "6rem 0rem 10rem 10rem" }}>
                    <img src={require("../assets/images/" + wea.list[0].weather[0].icon + ".png")} style={{ width: "25%" }} />
                    <div style={{ textAlign: "start", marginLeft: "4rem" }}>
                        <div style={{ color: "#000" }}>Today</div>
                        <h1 style={{ color: "#000" }}>{wea.city.name}</h1>
                        <div style={{ color: "#113555" }}>Temperature: {(wea.list[0].main.temp - 273.15).toFixed(2)}°C</div>
                        <div style={{ color: "#113555" }}>{wea.list[0].weather[0].description}</div>
                    </div>
                </div>
            </div>

            <div style={{ width: "80%", display: "flex", justifyContent: "space-between", marginTop: "-8rem", zIndex: "1" }}>
                {fourDayForecast.map((e) => {
                            return (
                                <div className="four-item">
                                    <h1 style={{ fontSize: "1.5rem" }}>
                                        {new Date(e.dt_txt).toLocaleDateString('en-EN', { weekday: 'long' })}
                                    </h1>
                                    <img src={require("../assets/images/" + e.weather[0].icon + ".png")} style={{ width: "5rem", margin: "1rem auto" }} />
                                    <div>{(e.main.temp - 273.15).toFixed(2)}°C</div>
                                </div>
                            );
                        })
                }
            </div>
        </div>
    );
}

export default WeatherResult;
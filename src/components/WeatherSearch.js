import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {fetchWeather, inputEnterFired, inputTextChangeAction} from "../actions/weather.action";
import { useDispatch, useSelector } from "react-redux";
import { TextField } from "@mui/material";
import WeatherResult from "./WeatherResult";

const WeatherSearch = () => {
    const dispatch = useDispatch();

    const { inputString, weather, pending, showResult } = useSelector((reduxData) => {
        return reduxData.weatherReducer;
    });

    const inputChangeHandler = (event) => {
        dispatch(inputTextChangeAction(event.target.value));
    }

    const onWeatherInputSubmit = (event) => {
        event.preventDefault();
        dispatch(inputEnterFired());
        dispatch(fetchWeather(inputString));
    }

    return (
        <div style={{display: "flex", justifyContent: "center"}}>
            {
                showResult ? <div>{pending ? <img src={require("../assets/images/loading.gif")} style={{marginTop: "10rem"}}/> : <WeatherResult wea={weather} />}</div>
                    : <div className="bg">
                        <div className="content text-center">
                            <div>
                                <div style={{ display: "flex", alignItems: "center", justifyContent: "center", marginTop: "5rem" }}>
                                    <img src={require("../assets/images/02d.png")} style={{ width: "25%", marginRight: "2rem" }} />
                                    <h1 style={{ color: "#365a7a", fontSize: "4rem", fontWeight: "700" }}>Weather Forecast</h1>
                                </div>

                                <form onSubmit={onWeatherInputSubmit}>
                                    <TextField sx={{ width: "65%" }} placeholder="Enter a City..." onChange={inputChangeHandler} value={inputString}
                                        InputProps={{
                                            style: {
                                                borderRadius: "20px",
                                                padding: "0rem 1rem",
                                                fontSize: "1.2rem",
                                                marginTop: "4rem",
                                                marginBottom: "4rem",
                                                backgroundColor: "transparent !important"
                                            }
                                        }}
                                    />
                                </form>
                            </div>
                        </div>
                    </div>
            }
        </div>
    );
}

export default WeatherSearch;
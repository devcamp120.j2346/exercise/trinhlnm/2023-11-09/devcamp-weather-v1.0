import { INPUT_ENTER_FIRED, INPUT_TEXT_CHANGE, WEATHER_FETCH_ERROR, WEATHER_FETCH_PENDING, WEATHER_FETCH_SUCCESS } from "../constants/weather.constant"

export const inputTextChangeAction = (inputValue) => {
    return {
        type: INPUT_TEXT_CHANGE,
        payload: inputValue
    }
}

export const fetchWeather = (cityName) => {
    return async(dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: WEATHER_FETCH_PENDING
            });

            const resWeatherNow = await fetch("https://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&appid=4cfe49e0cc90d98bc792c65d54d2e3ef", requestOptions);

            const dataWeatherNow = await resWeatherNow.json();

            const resWeather5 = await fetch("https://api.openweathermap.org/data/2.5/forecast?lat=" + dataWeatherNow.coord.lat + "&lon=" + dataWeatherNow.coord.lon + "&appid=4cfe49e0cc90d98bc792c65d54d2e3ef", requestOptions);

            const dataWeather5 = await resWeather5.json();

            return dispatch({
                type: WEATHER_FETCH_SUCCESS,
                data: dataWeather5
            });
        } catch (error) {
            return dispatch({
                type: WEATHER_FETCH_ERROR,
                error: error
            });
        }
    }
}

export const inputEnterFired = () => {
    return {
        type: INPUT_ENTER_FIRED
    }
}
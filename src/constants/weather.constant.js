export const INPUT_TEXT_CHANGE = "Input text change";

export const WEATHER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy thời tiết";

export const  WEATHER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy thời tiết";

export const  WEATHER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy thời tiết";

export const INPUT_ENTER_FIRED = "Phím Enter được ấn sau khi nhập xong input";